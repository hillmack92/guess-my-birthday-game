from random import randint

# build a game Guess My Birthday

# start with a prompt : "Hi! What is your name?"
# user inputs their name

name = input('Hi! Could you tell me your name please? ')
month_number = randint(1,12)
year_number = randint(1924,2004)

# prompt "Guess <guess number> : <name> were you born in <month>/<year>"
# prompt "yes or no?"
# user inputs yes or no

print('Guess 1: ', name, ' were you born in ', month_number, ' / ', year_number, '?')
response = input('Yes or No? ').lower()

# if yes,
# computer prompts "I knew it!" and guessing stops
# if no,
# computer prompts "Drat! Lemme try again!"

if response == 'yes':
  print('I knew it!')
  exit()
else:
  print('Drat! Lemme try again!')

month_number = randint(1,12)
year_number = randint(1924,2004)

print('Guess 2: ', name, ' were you born in ', month_number, ' / ', year_number, '?')
response = input('Yes or No? ').lower()

if response == 'yes':
  print('I knew it! Only took me two guesses!')
  exit()
else:
  print("I'll get it on my third guess!")

month_number = randint(1,12)
year_number = randint(1924,2004)

print('Guess 3: ', name, ' were you born in ', month_number, ' / ', year_number, '?')
response = input('Yes or No? ').lower()

if response == 'yes':
  print("I knew it! Told you I'd get it on my third guess!")
  exit()
else:
  print("Okay, I'm sure I'll get it on my fourth guess!")

month_number = randint(1,12)
year_number = randint(1924,2004)

print('Guess 4: ', name, ' were you born in ', month_number, ' / ', year_number, '?')
response = input('Yes or No? ').lower()

if response == 'yes':
  print('Aha! See, I knew it! Four guesses ain\'t bad!')
  exit()
else:
  print('I have other things to waste my time doing, later!')

# after 4 guesses
# computer prompts "I have other things to do. Good bye."